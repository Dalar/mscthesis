%!TEX root = /Users/amodig/Dropbox/Documents/MScThesis/thesis-arttu.tex
\chapter{Gaussian Channel Capacity}
\label{chapter:appendix}

\section{Channel Capacity}

The information channel capacity of a discrete \emph{memoryless} channel is
\begin{equation}
  C = \max_{p(x)} I(X;Y) = \max_{p(x)}\: [H(Y) - H(Y|X)] .
\end{equation} \label{eq:channel}

\begin{figure}[tbh]
  \centering
  \includegraphics[width=0.5\textwidth]{images/channels}
  \caption{Information channels after transmitting a message with $n$ symbols. The input space is $X^n$ and the output space is $Y^n$. Adapted from \citep{cover2012elements}.} \label{fig:channels}
\end{figure}

The operational meaning to the definition of a capacity $C$ is the number of bits one can transfer reliably over a channel. Because of innate channel noise, for each input $n$-sequence there are approximately $2^{nH(Y|X)}$ possible $Y$ sequences which are all approximately equally likely \cite{shannon1948}. This is represented in Figure \ref{fig:channels}. For communication purposes, we wish to ensure that no two $X$ sequences produce the same $Y$ output sequence over the channel. Otherwise we will not be able to decide which sequence $X$ was sent.

The total number of possible $Y$ sequences is approximately $2^{nH(Y)}$, represented by the right big ellipse in Figure \ref{fig:channels}. This set has to be divided into sets of size $2^{nH(Y|X)}$ which correspond to the different input $X$ sequences. Then the total number of disjoint sets is less than or equal to
\begin{equation*}
  \frac{2^{nH(Y)}}{2^{nH(Y|X)}} = 2^{n(H(Y)-H(Y|X))} = 2^{nI(X;Y)} .
\end{equation*}
Thus we can send only approximately $2^{nI(X;Y)}$ distinguishable sequences of length $n$. This outlines the upper bound of the channel capacity, but as Shannon's second theorem shows, any rate up to the bound (not necessarily inclusive) is achievable with an arbitrarily low probability of error. \cite{cover2012elements} \cite{shannon1948}


\section{Gaussian Channel} \label{section:gaussian_channel}

Let us suppose that information is sent over a channel that is subjected to additive white Gaussian noise. Then the output is
\begin{equation}
  Y_i = X_i + Z_i \,,
\end{equation}
where $Y_i$ is the channel output, $X_i$ is the channel input, and $Z_i$ is zero-mean Gaussian noise with variance $N$. The noise $Z_i \sim \mathcal{N}(0,N)$ is assumed to be independent of the signal $X_i$ and it is independent and identically distributed (i.i.d.). Thus the output (and input) can take on a continuum of values.

Without further conditions, the channel capacity of a Gaussian channel may be infinite. If the noise variance is zero, the channel can transmit an arbitrary real number with no error. With no constraints on the input, we can choose an infinite subset of inputs arbitrarily far apart, and they are distinguishable at the output with arbitrarily small probability of error (see Figure \ref{fig:channels}).

A common assumption is that the average power of the channel is constrained. This means that for any codeword $(x_1, x_2, \dotsc, x_n)$ transmitted over the channel, we require that
\begin{equation}
	\frac{1}{n} \sum^n_{i=1} x^2_i \leq P \,.
\end{equation}

We can then define the information capacity of the channel as the maximum mutual information between input and noisy outputover all distributions on the input that satisfy the power constraint. Expanding the $I(X;Y)$, we have
\begin{align}
  I(X;Y)  &= h(Y) - h(Y|X) \\
          &= h(Y) - h(X + Z|X) \\
          &= h(Y) - h(Z|X) \\
          &= h(Y) - h(Z) \,,
\end{align}
since $Z$ is independent of $X$. Because $Z_i \sim \mathcal{N}(0,N)$, the entropy of $Z$ is $h(Z) = \frac{1}{2} \log 2 \pi e N$ according to the Equation \eqref{eq:entropy-normal}. Also,
\begin{equation}
  \mathbb{E}[Y^2] = \mathbb{E}[(X+Z)^2] = \mathbb{E}[X^2] + 2\mathbb{E}[X]\mathbb{E}[Z] + \mathbb{E}[Z^2] = P + N ,
\end{equation}
since $X$ and $Z$ are independent and $\mathbb{E}[Z] = 0$. Then $h(Y)$ is bounded by $\frac{1}{2} \log 2 \pi e (P+N)$ because the normal distribution is the maximum-entropy distribution for a given variance. \cite{cover2012elements}

Applying this result to bound the mutual information, we get
\begin{align}
  I(X;Y)  &= h(Y) - h(Z) \\
          &\leq \frac{1}{2} \log 2 \pi e (P+N) - \frac{1}{2} \log 2 \pi e N \\
          &= \frac{1}{2} \log \left ( 1 + \frac{P}{N} \right) .
\end{align} 
Hence, the information capacity of the Gaussian channel is
\begin{equation}
	C = \max_{\mathbb{E}X^2 \leq P} I(X;Y) = \frac{1}{2} \log \left (1 + \frac{P}{N} \right) \,,
\end{equation}
which is attained when $X \sim \mathcal{N}(0,P)$ \cite{shannon1948}. It can also be shown that this capacity is also the supremum of the rates achievable for the channel. \cite{cover2012elements}

Paul Fitts described his seminal work \cite{fitts1954information} as the \emph{information capacity of the human motor system in controlling the amplitude of movement}. It was shown in Chapter \ref{chapter:fitts-law} that Fitts's paradigm has an information theory interpretation with self-information.

\begin{figure}
	\centering
	\includegraphics[width=0.66\textwidth]{images/spherepacking.png}
	\caption{Sphere packing for the Gaussian channel. \cite{cover2012elements}} \label{fig:spheres}
\end{figure}

The geometric plausibility of the aforementioned result can be shown as a sphere packing problem. For a codeword of length $n$, the received vector is normally distributed with mean equal to the true codeword and variance equal to the noise variance. With high probability, the received vector is contained in a sphere of radius $\sqrt{n(N+\epsilon)}$ because the vector falls within one standard deviation away from the mean in each direction, so the distance from the true codeword is the Euclidean distance $\sqrt{\mathbb{E}[z^2_1 + z^2_2 + \dotsb + z^2_n]} = \sqrt{nN}$. If we assign everything within this sphere to the given codeword, we misdetect only if the output vector falls outside the sphere. Other codewords will have other spheres each with radius approximately $\sqrt{n(N+\epsilon)}$. The received vectors are limited in energy, so they all must lie in a sphere of radius $\sqrt{n(P+N)}$. The volume of an $n$-dimensional sphere is of form $C_n r^n$, where $r$ is the radius of the sphere. The approximate maximum number of non-intersecting decoding spheres is therefore
\begin{equation}
  \text{number of spheres} = \frac{C_n (n(P+N))^{\frac{n}{2}}}{C_n (n(N+\epsilon)^{\frac{n}{2}}} \approx 2^{\frac{n}{2}(1+\frac{P}{N})} ,
\end{equation}
and the rate of code is thus $\frac{1}{2} \log (1+\frac{P}{N})$. This idea is illustrated in Figure \ref{fig:spheres}. This indicates that we cannot hope to send information at rates higher than $C$ with low probability of error.


