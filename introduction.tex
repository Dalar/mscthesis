%!TEX root = /Users/amodig/Dropbox/Documents/MScThesis/thesis-arttu.tex
\chapter{Introduction} \label{chapter:intro}

%\begin{itemize}
%  \item käytännön sovellukset
%  \item background: Fittsin lain rajoitteet
%  \item menetelmän hyödyt
%  \item menetelmän ongelmat
%  \item tulevaisuuden näkymät
%  \item tämän työn rakenne
%\end{itemize}

Measuring human's motor capabilities has always been a part of human culture. Since the dawn of sports, people have been competing with each other in skills that require a high level of motor-cognitive competence. Besides sports and other physical games, many artistic and musical skills, such as playing an instrument or performing a dance, require a high level of motorical capabilities. %Many world-class dancers, musicians and athletes are generally considered to be among the most motorically skilful people in the world. A sufficiently high level of motorical skill is usually deemed to be needed to be able to compete or play in the highest class of dancing, playing, and sports.

Evaluation of such skills is a whole discipline. In sports, where the scored performance is complex, there is a judge or a whole jury to evaluate the level of skill. In sophisticated competitions, there needs to be experts, usually expert performers themselves, that have the required knowledge to be able to evaluate others. Thus, there are virtually no objective ``hard'' metrics that could help with the evaluation process or even standardize it. The scoring process is commonly a jury of experts, relying more often to qualitative and subjective than quantitative and objective information. Measuring complex performance relies usually on keen human surveying that can be prone to errors and changes in perception.

In contrast, the modern world has changed into a data globe of vast quantitative information. Rate of communication and the exchange and extraction of information has never been faster and more efficient, which has also resulted in the explosion of new knowledge in various fields. The 21st century is perhaps best characterized by the emerging \emph{big data} in many fields, such as genomics, physics and Internet search. Processing the large and complex data sets is difficult, but the cost of such efforts is getting smaller, and valuable analysis is more and more feasible. Extracting knowledge from complex high-dimensional data sets is one of the key areas in modern statistics and machine learning.

Nonetheless, the entire so-called information age relies on the standard blocks of information: the bits. 1's and 0's have mathematically formalized many scientific concepts and the flow of information. Yet, it requires hard work to transform qualitative knowledge into quantitative, and to create general metrics that can translate the subjective knowledge into objective information.

This transitional problem is perhaps most clear in psychological and cognitive sciences. For example, most medical data is extractable through chemical or physical sensors, and as such, it is already feasible for evaluation, but creating strict objective tests to evaluate and standardize human performance is a difficult task. In many behavioral tasks, the problem is indeed very complex \cite{rasmussen1983skills, winter2009biomechanics}. Reflecting the triumph of computers and robots, many question also the human performance in similar metrics. What is the information processing in the human brain? What is the maximum rate of human communication? What is the information capacity of the human motor system?

In the wake of Claude E. Shannon's ground-breaking work on information theory in 1940s, there was a huge scientific interest to apply the theory in psychology \cite{attneave1959applications}, which Shannon regarded as a bandwagon effect \cite{shannon1956bandwagon}. Nonetheless, Paul Fitts made a fundamental observation in 1954 \cite{fitts1954information} that the rapid movement performance of an arm can be modeled in a prescribed task, and this mathematical model can also evaluate the information capacity of aimed movements. Even nowadays, \emph{Fitts' law} and its extensions remain as some of the few ``hard'' predictive models in human-computer-interaction, but the applications are very restricted.

In this thesis, I present and extend a novel research work \cite{oulasvirta2013information}, which attempts to create a general metric for measuring the performance rate, \emph{throughput}, of human motor control. The initial paradigm of Paul Fitts remains; since human responses are continuous variables, the information capacity is only limited by the statistical variability, or noise, of the responses. With the motion capture techniques of today, we have the tools to precisely track the continuous movements of a full human body. Thus, we have the data that enables us to estimate the information content of full-body motion available to an external viewer.

The applications of a general throughput metric for human motor expressiveness and control are tentatively vast. Besides as a general tool for human factors and human-computer interaction, we hypothesize that the metric could be also useful in sport science and medical science. For example, comparing to a reference movement given by a skilful \emph{t'ai chi} teacher, the metric would reveal the exact shortcomings in each student's repetitions. Or a clinical study, where the deterioration or improvement of patients with neuromotor impairments could be tracked exactly with a few motorical tests. The human perception has its limitations, but the precision in information world is in practice limitless. Even small deteriorations or variations in motor performance---that would otherwise escape human senses---can be revealed with high-performance modeling and information theory.

Despite the almost intuitive idea, creating a robust framework for the throughput metric is a hard problem to tackle. First, it requires sophisticated hardware and software to extract the motion data. Second, the motion data has to be standardized, which requires robust \emph{machine learning} algorithms. Third, the \emph{mutual information} has to be estimated. With state-of-the-art methods, the whole process can be very complex and slow. For fast analysis and evaluation, we have to use rough approximations that will overestimate the motion's inherent information content. But even with grossly overestimating the throughput values, the metric can be used to cross-compare and evaluate motions within similar style and data format.

The structure of this thesis is the following: In Chapter \ref{chapter:fitts-law}, as a background, I introduce the paradigm of Fitts' law. In Chapter \ref{chapter:mocap}, I describe the different methods of acquiring motion data of various kinds. In Chapter \ref{chapter:information-estimation}, I describe the different mathematical methods that are required in the estimation of motion's information. After these theoretical chapters, the implementation will be presented in Chapter \ref{chapter:implementation}. Then, the evaluation methods and results are presented in Chapter \ref{chapter:evaluation}. Finally, the discussion about the current framework, future work and prospects are in Chapter \ref{chapter:discussion}, along with the conclusions in Chapter \ref{chapter:conclusions}.



\chapter{Fitts' Law} \label{chapter:fitts-law}

%\section{The Information Capacity in Controlling the Amplitude of Movement\\-- The Fitts' Law} \label{section:fitts-law}

The human motor control is the process of using the neuromuscular system to activate and coordinate the muscles and limbs under the performance of certain motor skills. It is based on the integration of sensory information, both about the environment and the body, to determine the appropriate neuromuscular actions to generate some desired movement. As the process requires cooperation between the central nervous system and the musculoskeletal system, it is as such a problem of information processing, coordination, mechanics, physics, and cognition. Successful motor control is crucial to interacting with the environment, and a high performance of a person's general motor control and skills is advantageous.

Since Claude E. Shannon's information theory in the late 1940s, the theory was quickly applied in the fields of cognitive and behavioral sciences, in relation to sensory, perceptual, and perceptual-motor-functions. The work by Paul Fitts in 1950s extended the framework to human motor control \cite{fitts1954information, fitts1964information}, and it has become the dominant paradigm in studying the information capacity of the human motor system. The primary application of Fitts's paradigm, commonly known as \emph{Fitts' law}, is the analysis and evaluation of user interfaces in human-computer interaction (HCI) \cite{mackenzie1992, soukoreff2004towards, zhai2002validity, zhai2004characterizing}. For example, the paradigm was one main motivation in the development and adoption of the computer mouse over the joystick \cite{atkinson2007best, card1978evaluation}.

\section{Fitts's Paradigm}

In the pioneering work, Fitts was interested in \emph{discrete aimed continuous movements}, that is, movement where a pointer (arm, finger, display cursor, etc.) is moved onto a spatially expanded target. A common example is moving a mouse cursor on top of a button and clicking it on a computer display. Originally, one of Fitts' lab experiments \cite{fitts1954information} had a setup of a physical apparatus with metal plates and metal-tipped stylus pens, represented in Figure \ref{fig:fitts-tapping}. The instructions for the tapping were to hit the plates alternatively in a time limit, with emphasis on accuracy rather than speed. This long reciprocal tapping effectively minimized the hindering effect of motor-cognitive reaction time.

\begin{figure}
  \centering
  \includegraphics[width=0.50\textwidth]{images/fitts-tapping}
  \caption{Reciprocal tapping apparatus after \citet{fitts1954information}. The task was to hit the center plate in each group alternately without touching either side (error) plate.}
  \label{fig:fitts-tapping}
\end{figure}

Fitts's specific hypothesis was: \emph{the average time per response will be directly proportional to the minimum average amount of information per response demanded by the particular conditions of amplitude and tolerance}. Formulated as a linear relationship, the time per response is commonly dubbed as the \emph{movement time} (MT), and the amount of information per response, the number of bits to fulfill the task, is dubbed the \emph{index of difficulty} (ID):
\begin{equation} \label{eq:fitts-model-orig}
    \text{MT} = b \cdot \text{ID} \,.
\end{equation}
where $b$ is the unknown parameter. Fitts' original definition of index of difficulty $I_d$ was
\begin{equation} \label{eq:id-fitts-orig}
  I_d = -\log_2 \left ( \frac{W_S}{2A} \right ) \;\; \text{bits/response,}
\end{equation}
where $W_S$ is the target width and $A$ is the distance to the target or ``amplitude'' -- a rather confusing term which was used by Fitts. Equation \eqref{eq:id-fitts-orig} is now commonly known as
\begin{equation} \label{eq:id-fitts}
    \text{ID} = \log_2 \frac{2D}{W} \;\; \text{bits,}
\end{equation}
where $D$ is the distance to the target center, and $W$ is the width of the target. When $R=\frac{1}{2}W$, the ID can also be presented as
\begin{equation} \label{eq:id}
  \text{ID} = \log_2 \left ( \frac{D}{R}\right ) = \log_2 \left( \frac{D}{\Delta D} \right ) \, \text{bits,}
\end{equation}
where $D$ is the length of the required movement, and $\Delta D$ is the error or inaccuracy of the movement.

ID Equation \eqref{eq:id} in essence the information needed to complete an aiming task. According to the properties of \emph{self-information} (discussed in Section \ref{section:mutual_information}), the logarithm of the number of bins (which are distinct possible ``messages'') of uniform probability gives us the information needed when one of the bins is ``selected''. Under the Fitts's paradigm, the subject has at least the same efficiency of hitting all the bins as hitting the most difficult target bin. The paradigm is shown in Figure \ref{fig:tapping} where the target bin width is $W = 2R = 2 \Delta D$.

\begin{figure}
  \centering
  \includegraphics[width=0.75\textwidth]{images/tapping2crop}
  \caption{The Fittsian aiming task. Under the Fitts's model, it is possible to hit each bin with the same probability when sending a message. The motor capacity is determined, when the subject cannot hit targets farther than \emph{i} and \emph{vii} in the same time interval as $i$ and $vii$ with enough accuracy.}
  \label{fig:tapping}
\end{figure}


\section{Fitts' Law as an Evaluation Tool}

In the time of Fitts's original experiment, the true dispersion of the target hits and misses was not available, as Fitts only knew the error-rate in each procedural task. With the computer mouses, displays and touchscreens of nowadays, the dispersion of the target taps can be modelled more accurately than before. With this information, it is possible to calculate the effective width $W_e$ of the target, which is based on a certain factor of the standard deviation. Usually in the literature \cite{welford1968fundamentals, mackenzie2008throughput,zhai2004characterizing}, the factor is $W_e = \sqrt{2 \pi e} \sigma \approx 4.133 \sigma$, which is 96.12 \% of the weight of the normal probability distribution.

Using this effective width $W_e$ it is also possible to calculate the effective (or adjusted) index of difficulty $\text{ID}_e$. As effective width depends on the variance of the hits, which have the property to be Gaussian \cite{woodworth1899accuracy}, Fitts' law also relates that the speed-accuracy trade-off of motor-cognitive control is independent of the throughput as it is the constant motor control capacity in aiming tasks of the same type. This has been proved empirically by \citet{fitts1966information} and \citet{mackenzie2008throughput}.

Fitts's experimental setup had no reaction times involved, but in an experiment where reaction times are involved, a bias time term $a$ has to be added. Thus, the model has the popular form and is known as the \emph{Fitts' law}:
\begin{equation} \label{eq:fitts-law}
    \text{MT} = a + b \cdot \text{ID} \,.
\end{equation}
The term $a$ can also take into account factors, that are not modelled by the Fittsian aiming task itself.

Fitts coined the term \emph{index of performance} (IP) as the performance rate in bits per second:
\begin{equation} \label{eq:ip1}
    \text{IP} = \frac{\text{ID}}{\text{MT}} \,.
\end{equation}
which used the original model \eqref{eq:fitts-model-orig} which does not have the reaction time term $a$, thus defining IP as
\begin{equation} \label{eq:ip2}
  \text{IP} = \frac{1}{\text{MT}} \log_2 \frac{2D}{W} \, \text{bits/s,}
\end{equation}
where MT is the movement time per response, $W$ is the width of target and $D$ the distance to the target. IP is in essence the rate of performance ability, also called as the \emph{throughput}. From the linear regression problem \eqref{eq:fitts-law}, the throughput in a Fittsian movement task is usually defined as
\begin{equation} \label{eq:tp}
    \text{TP} = \frac{1}{b} \, \text{bits/s},
\end{equation}
though an International Standard \textsl{ISO 9241-9} was created which defines throughput also as
\begin{equation} \label{eq:iso-tp}
    \text{TP} = \frac{\text{ID}}{\text{MT}} \,.
\end{equation}
The distinction between these lies in the premiss whether the parameter $a$ in seconds---often denoted as the reaction time in the experimental task---is part of the Fittsian movement time, thus whether $a$ affects the throughput that is estimated from a regression problem.

When Fitts' law Equation \eqref{eq:fitts-law} is used in a linear regression problem, the throughput $1/b$ can be approximated in an experiment with a least-squares method, where the relationship between the difficulty of an aiming task (ID) and the time to complete a particular response (MT) can be observed. In the literature this has been a standard method of evaluating a certain input device based on continuous aiming \cite{card1978evaluation,mackenzie1992,soukoreff2004towards}. For example, selecting targets with a mouse yields a range of throughput of 3.7--4.9 bps while selecting targets with a touchpad yields a range of 1.0--2.9 bps \cite{soukoreff2004towards}.


\section{Shannon Formulation}

A notion of worth has to be said about the possible premiss which Fitts had, that the decision to not move at all does not convey any information, that is, selecting the bin where the hand is already placed does not require any additional information (see Figure \ref{fig:tapping}). In contrast to the Shannon's \emph{channel capacity} theorem, all distinct possible messages count. Therefore, as an analogue to Shannon's distinct codewords, the number of possible bins (messages) is $(D+W)/W$, and the index of difficulty becomes
\begin{equation} \label{eq:id-shannon}
	\text{ID}_{\text{Shannon}} = \log_2 \left ( \frac{D}{W} +1 \right ) \,,
\end{equation}
and the throughput (without the time bias $a$) becomes
\begin{equation} \label{eq:tp-shannon}
  \text{TP}_{\text{Shannon}} = \frac{1}{\text{MT}} \log_2 \left ( \frac{D}{W} +1 \right ) \,.
\end{equation}

\citet{mackenzie1989note} was first to adopt this formulation, though according to MacKenzie, it was based on a ``direct analogy'' of Shannon's Theorem 17 (Shannon--Hartley theorem)~\cite{shannon1948}, the channel capacity of a communications channel:
\begin{equation} \label{eq:capacity}
  C = W \log_2 \left ( \frac{P}{N} + 1 \right ) \,,
\end{equation}
where $W$ is the bandwidth of the channel in hertz, $P$ is the transmitter power, and $N$ is the noise power. Compare this to Shannon-TP Equation \eqref{eq:tp-shannon}. MacKenzie dubbed his version of Fitts' law as the Shannon model:
\begin{equation} \label{eq:mt-shannon}
	\text{MT}_{\text{Shannon}} = a + b \log_2 \left ( \frac{D}{W} +1 \right ) \,.
\end{equation}
Though Fitts mentioned Shannon's Theorem 17 in the original paper \cite{fitts1954information}, he was for some reason reluctant to adapt it directly, perhaps for the conceptually difficult implication that a ``no-movement'' could be also interpreted as a message.

This \emph{Shannon formulation} has been a common alternative formulation of Fitts' law in the HCI literature especially after \citet{mackenzie1991}. Although the similarity of Equations \eqref{eq:tp-shannon} and \eqref{eq:capacity} is prominent, the real justification of Equation \eqref{eq:mt-shannon} has been criticised by \citet{drewes2010} and \citet{hoffmann2013}. Nevertheless, \citet{mackenzie2013note} defended the empirical validity of the Shannon version.

In this thesis, the Shannon's sphere packing analogy to channel capacity is presented in Appendix \ref{section:gaussian_channel}, and it can be argued that the Shannon-ID \eqref{eq:id-shannon} is also justified, as it reflects the general difficulty of the aimed pointing task---the motor-cognitive output information needed to solve the task---which is logarithmically proportional to the number of distinct possible messages the subject can send. This was also Fitts's original idea as the information theoretical basis \cite{fitts1954information}.

Initiative work on the exploration of differentiable motion space has been done by \citet{williamson2012rewarding}, though in which the focus was on identifying motion \emph{variability} in the full joint user-sensor motion space, here the focus is on motion \emph{reliability} and \emph{complexity} in similar movement tasks.


\section{Extensions and Restrictions of Fitts' law}

Fitts' law has been extended into continuous aimed movements, which first covered just the path width and length \cite{accot1997beyond, accot1999performance}, but were later extended also to curvature \cite{liu2010effect}. However, these extensions to Fitts' law have no interpretation in information theory. Some research has also considered the information capacity of bi-manual control \cite{marteniuk1984bimanual}.

The general restriction of Fitts' law paradigm and its extensions is that the experimental conditions are \emph{prescribed} to a high degree by the experimenter, and limited to just few body parts. Also, the information capacity can be evaluated only from aimed movements where just the end points matter. Thus, the information being measured is tantamount to the subject's ability to motorically conform to the extrinsic constraints, excluding entirely free movement that is irrespective of its absolute position and where the goal is to produce a certain shape or pattern. Many skilled activities also utilize multiple body parts or the full body, such as dance and sports. Most also involve compound tasks where multiple types of movement are performed simultaneously and sequentially.

Due to these limitations, it is easy to see that the Fitts' law paradigm is not suitable for the study of skilled motor action; the action that can be expected to contain and communicate the most information. Therefore, the main motivation of this thesis work is to create a new paradigm based in information theory that would take into account motion that has multiple and continuous movement points, and where the emphasis is on shapes and patterns created by the subject herself, with as few experimental restrictions as possible.


%\section{Shannon's information}

%Theorem 17 from \citet{shannon1948}:



%\section{Structure}\label{section:structure}