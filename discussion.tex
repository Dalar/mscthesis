%!TEX root = /Users/amodig/Dropbox/Documents/MScThesis/thesis-arttu.tex

\chapter{Discussion} \label{chapter:discussion}

This thesis, along with and extending the work by \citet{oulasvirta2013information}, has presented a novel metric for measuring the information capacity of full-body movements. The new metric has been inspired the work by \citet{fitts1954information} in the 1950s, and is thus an extended and generalized re-creation of Fitts' law paradigm. It shares the intuitive premiss by Paul Fitts that the \emph{``information capacity is limited only by the amount of statistical variability, or noise, that is characteristic of repeated efforts to produce the same response''}. However, the discrete restrictions of Fitts' law are surpassed by considering
\begin{itemize}
	\item the shape of a continuous movement trajectory as the source of information instead of the target width and distance, and
	\item the inaccuracy of the reproduced movement as the source of noise instead of end-point variation.
\end{itemize}

The Fittsian metric is basically a version of the minimum amount of \emph{self-information} which an aiming task needs to be solved. As Fitts remarked, the index of difficulty specifies \emph{the minimum information required on the average for controlling or organizing each movement}, and Fitts postulated an index of difficulty for simple amplitudal movement.

In contrast, the metric described in this thesis defines the \emph{mutual information} of a reproduced task, which can be a more accurate metric in measuring the whole theoretical motor capacity, but can be grossly overestimated when the latent space of the motion is completely up to the subject herself with all available degrees of freedom, and thus the space is difficult to define. Nonetheless, we may assume that in a Fittsian aiming tasks, the ``true'' information capacity lies somewhere between the Fitts's TP and ours.

The metric allows the examination of any scenario wherein users' motion can be represented as time series features, from mouse movements to full-body motion. Any kind of movement data can be analyzed, with the only requirement being the repeatability of the movement. In contrast, the known extensions of Fitts' law from discrete to continuous movements are predictive models of the movement time \cite{accot1999performance, liu2010effect}, and do not carry an interpretation in information theory, nor can deal with multi-feature arbitrary trajectories in 3D space.

As the metric is based on estimation of \emph{mutual information} in viewed movements, it should not be confused with the intrinsic difficulty of performing the movement nor with the neuromotor system's information capacity. In reality, relatively simple motor-cognitive tasks can produce high throughput values, and some relatively complex feats, such as balancing, produce zero throughput. As such, the metric is best understood as the rate of information available to an external observer from the sensor space, defined through the complexity and reproducibility of captured movements.

Although, as the metric is a serious generalization of Fitts's TP, it lacks the predictive capability of the Fitts's paradigm. As the Fitts's TP is proportional to the index of difficulty, it is relatively robust to the changes in the task parameters, $W$ and $D$, under the Fitts' law. With more complex and variable motor control tasks, interpolation and extrapolation cannot be expected, because free-form movement directions may invoke very different control patterns even with slight changes in trajectories.

The only assumption the metric makes about the data is a constant framerate. This makes it suitable for a wide range of uses, for example, researchers can do analysis on touchscreen and MOCAP data in a similar way. On the other hand, the absolute TP values in the data space can be extremely high compared to the familiar TP ranges in Fitts' law literature. However, the high range is expected because the metric may use high-frequency multi-feature sampling of continuous movements, and also because the model has no prior models of the performer or the environment that would restrict the degrees-of-freedom. It has been shown that the metric's absolute estimates can be greatly reduced by searching for the latent motion space using unsupervised machine learning, although robust automatic nonlinear dimension reduction is still problematic.


\section{Applications}

The metric has many foreseeable user cases, but in this thesis work, mainly the \emph{analysis of motor control and performance} has been shown to be possible. In the Chapter \ref{chapter:evaluation} about evaluation, it was shown that the metric is sensitive to some well-known effects in motor control, such as laterality, encumbered movement, change in performance objective such as speed, and perceptual distraction of bimanual control. As such, the metric is a good tool for sensitive and objective analysis. The metric can also be used to analyze the contributions of different limbs in users' full-body movement control, and to expose the performance-affecting factors. The timing and synchrony of motions can be explored by removing the temporal alignment (time warping) step.

Other possible user cases for the metric are related to user interface research and exploration. For example, different input methods or interface designs which share the same sensor space can be compared with respect to the available motor capacity. Using multiple complex reference gestures that are reproducable with each user interface design, researchers can compare either novel or alternative solutions. As most movement spaces are too large to be examined exhaustively, complex overlearned patterns such as signatures can be used to represent performance that users could attain with considerate practise.

Also, the metric could provide a useful tool for user interface exploration. Essential input mechanisms need to be distinguishable and controllable. This implies high variability and reliability of the gestures. If gesture movements have a variation which is not under the control of a user, the information capacity of the channel will be diminished as some movements will be indistinguishable.

The metric also incorporates recognisability with complexity, which is valuable in authentication. The security of free-form gesture \emph{passwords} would be limited by the achievable throughput, when the reference gesture is saved to the device memory. The security and memorability of such password authentication system has been recently studied by \citet{sherman2014user}, showing that the metric could provide a sensible authentication threshold for free-form passwords.

% The metric has many foreseeable user cases, but in general, they can be categorized into three groups:
% 
% \begin{description}
%   \item[1. Analysis of motor control and performance] \hfill \\
%   In the evaluation, it ws shown that the metric is sensitive to some well-known effects in motor control, such as laterality, encumbered movement, change in performance objective such as speed, and perceptual distraction of bimanual control. As such, the metric is a good tool for sensitive and objective analysis. The metric can also be used to analyze the contributions of different limbs in users' full-body movement control, and to expose the performance-affecting factors. The timing and synchrony of motions can be explored by removing the temporal alignment step (CTW).
%   
%   \item[2. Evaluation and comparison of user interfaces] \hfill \\
%   The metric can be used to study the motor capacity allowed by novel user interface designs and to compare alternative solutions. Comparative studies should target to obtain a large number of comparable complex gestures produced with each user interface. Because most movement spaces are too large to be exhaustively examined, complex overlearned patterns such as signatures can be used to represent performance that users could attain with considerate practise.
%     
%   \item[3. User interface exploration] \hfill \\
%   Because the metric is independent from intermediary devices and target conditions, it allows the exploration of potential user interfaces and gestures. For example, if there are multiple open ideas how a game could be controlled, the interface options could be compared through examining the capacity of reproducement within each condition.
% \end{description}

To help users to apply the metric, there is a dedicated Web service at \url{http://infocapacity.hiit.fi/}. It enables the users to make computations with the metric without the need for installing any software. The interface provides an ease of use even without knowledge in programming, as the user can select the most important parameters through the Web client. The results are then sent to the user by email.


\section{Future Work}

%\begin{itemize}
%	\item yhtenäinen malli
%  \item algoritmien nopeuttaminen, scalability
%  \item klusterin käyttö serverillä
%  \item metriikan lisätutkimus, esim. salasanat
%\end{itemize}

The most important goal for future work is to combine the currently separate steps of complexity estimation, temporal alignment, and dimension reduction under a single robust model. Possible cues are Shared GP-LVM with \emph{manifold relevance determination} \cite{ek2009shared, damianou2012manifold}, Bayesian filtering incorporation similar to GP-BayesFilters \cite{ko2011learning}, and dynamical spatio-temporal GP regression \cite{solin2016} with a dimension reduction \cite{dewar2009, scerri2009, zammit2012}.

Unlike the Fittsian TP, the computation process is also very intensive and time-consuming with CTW and GP-LVM, and especially so if the motion sequences are long. Approximate Bayesian inference methods would improve the speed of computation, but GP-LVM still has fundamental problems in scalability, although many fast inference methods exist for GP regression and classification with \emph{expectation propagation} \cite{jylanki2013approximate} and \emph{stochastic variational inference} (SVI) \cite{hensman2013gaussian}. One promising direction is introducing SVI to GP-LVM, but the implementation is tricky \cite{hensman2013gaussian}. Another efficient option is using distributed variational inference through MapReduce algorithm, but full utilization requires the availability of hundreds of CPU cores with a suitable framework \cite{gal2014distributed, dean2008mapreduce}.

Alternative choice for dimension reduction would be to use non-parametric mutual information estimation, which also eases the presumption of a certain joint distribution family for the trajectory residuals. One popular approach is \emph{$k$-nearest-neighbour} (kNN) based estimation by \citet{kraskov2004estimating}, but this was shown to be inaccurate for measuring strong dependencies \cite{shuyang2015MI}. The proposed correction by \citet{shuyang2015MI} doesn't assume \emph{local uniformity} of the density over a region containing $k$ nearest neighbours of a particular point, but it relies on the \emph{local linearity} over the same region.

The estimation of trajectory complexity with second-order autoregressive model is very naive. Better results could be achieved with Bayesian estimation filters for dynamic \emph{state-space models}, such as a \emph{Kalman filter} \cite{kalman1960new}. As the transformation related to motion dynamics and general noisy MOCAP data are linear, it is possible to estimate the variance of dynamics with maximum likelihood estimation and derive recursive \emph{sensitivity equations} \cite{gupta1974computational, vaananen2012gaussian}.

Even further improvement could be achieved if the model's state-space is informed with a skeletal model with degrees of freedom and movement ranges of bones and joints. The lower bound of information capacity could also be researched through biomechanical simulations, where the inputs are strictly controlled.

The metric itself needs more studies and evaluation in its potential. The preliminary results presented here need additional validation with more test subjects. Application in sport sciences include training of complex motor schemas with reference motions. Potential new diagnostic tools could be applied in clinical health care, for example, changes in the motor capacity could offer an early identification process of neurological disorders related to motor dysfunction, and monitoring the neuroplastic recovery after lesions and other impairments. Also, applications in user authentication have already been studied where free-form motion ``signatures'' could be stored as passwords and the metric would enable both identification and a measure for the password strength \cite{sherman2014user}. In theory it would enable a very strong personalised password software, the only limit being a person's motor capacity and memory, but a feasible, robust and comprehensive framework for e.g. mobile devices needs much more evaluation and refinement.
