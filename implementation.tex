%!TEX root = /Users/amodig/Dropbox/Documents/MScThesis/thesis-arttu.tex

\chapter{Implementation} \label{chapter:implementation}

\section{Capturing of Motions and Gestures}
\subsection{Vicon and PhaseSpace}
Traditionally, motion capturing has been based on physical markers attached on the body of the performer, and capturing full-body motion has required multiple cameras with high accuracy. These optical systems are usually divided in systems based on \emph{passive markers} and in systems based on \emph{active markers}. Systems with passive markers use markers coated with retroreflective materials (reflecting light with minimal scattering), while systems with active markers typically use illuminated LEDs. Marker-based systems typically require that the performer wears a special suite made from e.g. spandex or lycra, to which markers are attached by velcro. The suite might hinder the motion, so during the dance study, no suite was used as the markers could be attached directly to the performers clothes or skin.

The study for ballerina dance sequences was captured using \emph{Vicon} motion capture system. The system consisted of 12 \emph{F-40} cameras with IR optical filters and IR LEDs, several processor units, a set of reflective dot markers, and \emph{Vicon Nexus} software. The system was able to capture 3D trajectories in submillimeter accuracy with a steady framerate of 120 fps. Trajectories were considered to be of high quality, that with minimal interpolation provided by the Vicon software, no further filtering was needed.

The study for bimanual in-air gesturing (The \emph{Minority Report} scenario) was captured using \emph{PhaseSpace} motion capture system. It consisted of 12 \emph{Impulse} cameras, a processor unit, and a set of active LED markers. The system was able to capture 3D trajectories with a steady framerate of 120 fps. Trajectories contained some errors and visible instrumental noise, so interpolation and smoothing were needed before analysis.

\subsection{Mouse Traces}
The study for cyclical tapping was captured using a standard optical mouse. Software for the test was custom-made using \emph{Pygame} Python modules designed for writing video games \cite{pygame}. The aimed framerate for capturing traces was 100 fps but due to technical shortcomings, the average delay between samples was 10.4 ms, which results to an average sample rate of 96.15 Hz. The traces acquired were considered to be of good quality, so no further preprocessing was needed before analysis.

\subsection{Microsoft Kinect}
\emph{Kinect} is a motion sensing input device developed by Microsoft for the Xbox~360 video game consol and Windows~PCs. The device features an RGB camera, IR depth sensor and a multi-array microphone. The depth sensor consists of an IR laser projector combined with a monochrome CMOS sensor. The depth sensor works under any ambient light conditions. This 3D range camera technology was developed by PrimeSense.

Microsoft's official non-commercial Kinect SDK has Windows compatible PC drivers for Kinect device \cite{KinectSDK}. Several open source framework alternatives are also available, the primary being \emph{OpenNI Framework} \cite{OpenNI} and \emph{Open\-Kinect} (libfreenect) \cite{OpenKinect}. The main framework for the implementation needed to be cross-platform and have efficient skeleton tracking libraries, so the most suitable framework was OpenNI with NITE middleware provided by PrimeSense.

OpenNI and NITE are able to track 15 different joints in the body. The GUI for an interactive demo software was made with \emph{Processing} programming language \cite{Processing} which also has a simple OpenNI and NITE wrapper \emph{simple-openni}. This allowed fast developping for an interactive demo with Kinect.

\section{Preprocessing of the Data} \label{section:preprocessing}
%\begin{itemize}
%    \item Smoothing
%    \item Interpolation
%    \item Cut
%\end{itemize}

MOCAP devices have always some instrumental noise that affects measurements, therefore some filtering as a preprocessing step is always needed for the raw data. In the MOCAP literature for kinematics, a Butterworth lowpass filter with a cut-off frequency of 6--9 Hz has been found simple yet effective \citep{winter1974measurement, winter2009biomechanics}. With several trials by judging the visible quality of MOCAP features, a cut-off frequency of 7 Hz was chosen for the raw data collected from the PhaseSpace system. The Vicon system natively incorporates very effective filtering, so no additional preprocessing was needed in that case.

Also, MOCAP markers are sometimes occluded from the cameras' viewpoints, especially in the case of motions where the limbs are close to the torso, or close to each other. Therefore there is usually some missing information. For short missing time frames, a piecewise cubic spline interpolation was used. If just a couple components had long intervals with missing information (10 frames or more with 120 fps) a straightforward linear interpolation was used. In a case of extensive missing information with multiple components, the whole motion sequence was deemed unusable.

Quite often there is missing information at the start or at the end of motion sequence data in one or several components. Then it is most practical to properly cut off all the time frames which had components with missing information. Also, frames that contain unnecessary information---when the motion under examination has not yet started or has ended---need to be cut away because including unnecessary frames naturally affects the throughput.


\section{Algorithms}
%\begin{itemize}
%    \item Canonical Time Warping
%    \item Variational GP-LVM
%    \item Shared information
%    \item Throughtput
%\end{itemize}

\emph{Canonical time warping} (CTW) was used for aligning two motion sequences accurately in a spatio-temporal manner. CTW combines \emph{canonical correlation analysis} (CCA) with \emph{dynamic time warping} (DTW) and extends CCA by allowing local spatial deformations. Basically, CTW finds the temporal alignment that maximizes the spatial correlation between two motion samples (see Section \ref{section:CTW}).

Alignment indices provided by CTW were used to duplicate the frames in each of the pair sequences locally. In effect, CTW is used to provide ``lag'' in each of the sequences when needed. The \textsc{Matlab} algorithm code was provided by \citet{CTW}. For these experiments, CTW was run with the default parameters suitable for MOCAP from the example code.

\emph{Gaussian process latent variable model} (GP-LVM) was used for the dimension reduction for motion data. Fully Bayesian GP-LVM provides automatical model selection for the amount of latent dimensions (\emph{automatic relevance determination}, ARD) with an RBF-ARD kernel. The model also integrates time dynamics as a prior variable. The algorithm was run with 100 inducing points, 50 initialization iterations (with fixed $\beta$-parameter) and a maximum of 1000 optimization iterations. The extensive \textsc{Matlab} software was provided by \citet{lawrenceGPsoftware}.

Alternatively, for ease and speed, \emph{principal component analysis} (PCA) was used for dimension reduction (see Section \ref{section:PCA}). According to a common practise, the number of principal components was chosen to explain 90\% of total variance.

The software for shared information and throughput computation were implemented in R code. \emph{Rscript} provides convenient interface for running the computations directly from \textsc{Unix} shell. The software is freely available on the project website: \url{http://infocapacity.hiit.fi}.

\section{Server Implementation}
%\begin{itemize}
%    \item LAMP
%    \item PHP web site
%    \item Matlab and R
%    \item Message queue: Beanstalk
%    \item Daemonizing: Supervisor
%\end{itemize}

A public Internet service for evaluating throughputs with a convenient GUI was implemented on a Linux Debian-based LAMP server (\textbf{L}inux, \textbf{A}pache, \textbf{M}ySQL, \textbf{P}HP). The PHP web site enables the user to input either two data files, a directory of paired gestures, a directory of repetitions, or a directory with sub-folders of repetitions of different gestures. The available optional procedures are an alignment of the pairs of data with CTW, a dimension reduction with either PCA or Bayesian GP-LVM, and throughput evaluation with different FPS values. The user then receives the results by an email.

The computation algorithms are implemented in \textsc{Matlab} and R, with a shell script that executes the commands in the server. Because the service has to enable multiple users at the same time, there needs to be a job queue for distributing the computation tasks one at a time in correct order without throttling the server, especially in the case of computationally heavy GP-LVM.

There are multiple open source software for job scheduling with time-consuming tasks. Simplicity and PHP implementation in mind, \emph{Bean\-stalk}~\cite{Beanstalk} program was chosen for an appropriate work queue manager with \emph{Phean\-stalk}~\cite{Pheanstalk} as the PHP client. Because Beanstalk only handles the messages between processes, the message-listening worker process had to be daemonized. \emph{Supervisor}~\cite{Supervisor} program was chosen to run the worker in the background. Both Beanstalk and Supervisor provide convenient interfaces for process management, event logging, and memory management in case of memory leaks. The web front-end of the service is in Figure \ref{fig:infocap-service}.

\begin{figure}[tbh]
  \centering
  \includegraphics[width=\textwidth]{images/infocap-service}
  \caption{The Internet service for calculating the information capacity.} \label{fig:infocap-service}
\end{figure}